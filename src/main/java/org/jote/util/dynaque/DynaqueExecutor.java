package org.jote.util.dynaque;

import org.jote.util.dynaque.parametervalue.DefaultParameterValues;
import org.jote.util.dynaque.parametervalue.OverrideParameterValues;
import org.jote.util.dynaque.parametervalue.RegularParameterValues;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 11/6/12
 * Time: 6:57 PM
 * To change this template use File | Settings | File Templates.
 */
public interface DynaqueExecutor<T, ID extends Serializable> {
    public DynaqueResult<T> dynamically(String name,
                                        Sort sort,
                                        Pageable pageable,
                                        RegularParameterValues regulars,
                                        DefaultParameterValues defaults,
                                        OverrideParameterValues overrides);

    public DynaqueResult<T> countDynamically(String name,
                                             Sort sort,
                                             Pageable pageable,
                                             RegularParameterValues regulars,
                                             DefaultParameterValues defaults,
                                             OverrideParameterValues overrides);

    public DynaqueResult countDynamically(DynaqueParameter parameter);

    public DynaqueResult dynamically(DynaqueParameter parameter);

    public EntityManager getEntityManager();
}
