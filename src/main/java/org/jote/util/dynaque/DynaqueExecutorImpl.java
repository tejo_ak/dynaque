package org.jote.util.dynaque;

import org.apache.commons.lang.StringUtils;
import org.jote.util.dynaque.annotation.Dynaque;
import org.jote.util.dynaque.annotation.Dynaquery;
import org.jote.util.dynaque.parametervalue.DefaultParameterValues;
import org.jote.util.dynaque.parametervalue.OverrideParameterValues;
import org.jote.util.dynaque.parametervalue.RegularParameterValues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.PersistenceProvider;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 11/7/12
 * Time: 2:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class DynaqueExecutorImpl<T, ID extends Serializable>

        extends SimpleJpaRepository<T, ID> implements DynaqueExecutor<T, ID>, Serializable {
    private static final long serialVersionUID = 1L;
    //    static Logger logger = (DynaqueExecutorImpl.class);
    private static final String WHERE = "where";
    private final JpaEntityInformation<T, ?> entityInformation;
    private final EntityManager em;
    private final PersistenceProvider provider;
    Logger logger = LoggerFactory.getLogger(DynaqueExecutorImpl.class);
    private Class<?> springDataRepositoryInterface;

    /**
     * Creates a new {@link SimpleJpaRepository} to manage objects of the given
     * {@link JpaEntityInformation}.
     *
     * @param entityInformation
     * @param entityManager
     */
    public DynaqueExecutorImpl(JpaEntityInformation<T, ?> entityInformation,
                               EntityManager entityManager, Class<?> springDataRepositoryInterface) {
        super(entityInformation, entityManager);
        this.entityInformation = entityInformation;
        this.em = entityManager;
        this.provider = PersistenceProvider.fromEntityManager(entityManager);
        this.springDataRepositoryInterface = springDataRepositoryInterface;
    }

    /**
     * Creates a new {@link SimpleJpaRepository} to manage objects of the given
     * domain type.
     *
     * @param domainClass
     * @param em
     */
    public DynaqueExecutorImpl(Class<T> domainClass, EntityManager em) {
        this(JpaEntityInformationSupport.getMetadata(domainClass, em), em, null);
    }

    public Class<?> getSpringDataRepositoryInterface() {
        return springDataRepositoryInterface;
    }

    public void setSpringDataRepositoryInterface(
            Class<?> springDataRepositoryInterface) {
        this.springDataRepositoryInterface = springDataRepositoryInterface;
    }

    @Override
    public DynaqueResult<T> dynamically(String name,
                                        Sort sort,
                                        Pageable pageable,
                                        RegularParameterValues regulars,
                                        DefaultParameterValues defaults,
                                        OverrideParameterValues overrides) {

        boolean isDynaqueInstance = DynaqueExecutor.class.isAssignableFrom(getSpringDataRepositoryInterface());
        if (isDynaqueInstance) {
            Annotation dynaqueAnno = getSpringDataRepositoryInterface().getAnnotation(Dynaque.class);
            if (dynaqueAnno != null) {
                Dynaque dynaque = (Dynaque) dynaqueAnno;
                List<Dynaquery> queries = Arrays.asList(dynaque.dynaquery());
                for (Iterator<Dynaquery> iterator = queries.iterator(); iterator.hasNext(); ) {
                    Dynaquery query = iterator.next();
                    if (StringUtils.equals(query.name(), name)) {
                        logger.debug("query found, let's execute");
                        DynaqueParameter parameter = new DynaqueParameter(name, sort, pageable, defaults, overrides, regulars);
                        DynaqueEngine de = new DynaqueEngine(query, parameter);
                        logger.debug(String.format("dynamically execute fetch query :%s", query));
                        Query q = de.fetch(em);
                        logger.debug("query fetched");
                        List<T> content = q.getResultList();
                        DynaqueResult<T> dr = new DynaqueResult<T>(parameter, content);
                        return dr;

                    }
//                    else {
//                        throw new DynaqueryNotfoundException("Dynaquery ['" + query.name() + "']not found on this repository");
//
//                    }
                }
            }
        }
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public DynaqueResult<T> countDynamically(String name,
                                             Sort sort,
                                             Pageable pageable,
                                             RegularParameterValues regulars,
                                             DefaultParameterValues defaults,
                                             OverrideParameterValues overrides) {
        return countDynamically(new DynaqueParameter(name, sort, pageable, defaults, overrides, regulars));  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public DynaqueResult countDynamically(DynaqueParameter parameter) {
        //trust the parameter
        boolean isDynaqueInstance = DynaqueExecutor.class.isAssignableFrom(getSpringDataRepositoryInterface());
        if (isDynaqueInstance) {
            Annotation dynaqueAnno = getSpringDataRepositoryInterface().getAnnotation(Dynaque.class);
            if (dynaqueAnno != null) {
                Dynaque dynaque = (Dynaque) dynaqueAnno;
                List<Dynaquery> queries = Arrays.asList(dynaque.dynaquery());
                for (Iterator<Dynaquery> iterator = queries.iterator(); iterator.hasNext(); ) {
                    Dynaquery query = iterator.next();
                    if (StringUtils.equals(query.name(), parameter.getName())) {
                        //Found query:
                        DynaqueEngine de = new DynaqueEngine(query, parameter);
                        logger.debug(String.format("dynamically execute fetch query :%s", query));
                        Query q = de.count(em);
                        List<Long> total = q.getResultList();
                        List<T> content = new ArrayList<T>();
                        DynaqueResult<T> dr = new DynaqueResult<T>(parameter, content);
                        dr.setTotal(total.size() > 0 ? total.get(0) : 0L);
                        return dr;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public DynaqueResult dynamically(DynaqueParameter parameter) {
        logger.debug("start calling dynamically by parameter");
        return dynamically(parameter.getName(),
                parameter.getSort(),
                parameter.getPageable(),
                parameter.getRegulars(),
                parameter.getDefaults(),
                parameter.getOverrides());  //To change body of implemented methods use File | Settings | File Templates.
    }

    public EntityManager getEntityManager() {
        return em;
    }
}
