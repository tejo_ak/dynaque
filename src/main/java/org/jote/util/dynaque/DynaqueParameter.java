package org.jote.util.dynaque;

import org.jote.util.dynaque.parametervalue.DefaultParameterValues;
import org.jote.util.dynaque.parametervalue.OverrideParameterValues;
import org.jote.util.dynaque.parametervalue.RegularParameterValues;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 11/7/12
 * Time: 8:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class DynaqueParameter {
    private Sort sort;
    private String name;
    private Pageable pageable;
    private DefaultParameterValues defaults;
    private OverrideParameterValues overrides;
    private RegularParameterValues regulars;

    public DynaqueParameter() {
    }

    public DynaqueParameter(String name,
                            Sort sort,
                            Pageable pageable,
                            DefaultParameterValues defaults,
                            OverrideParameterValues overrides,
                            RegularParameterValues regulars) {
        this.name = name;
        this.sort = sort;
        this.pageable = pageable;
        this.defaults = defaults;
        this.overrides = overrides;
        this.regulars = regulars;
    }

    public DynaqueParameter(String name,
                            DefaultParameterValues defaults,
                            OverrideParameterValues overrides,
                            RegularParameterValues regulars) {
        this.name = name;
        this.defaults = defaults;
        this.overrides = overrides;
        this.regulars = regulars;
    }

//    public DynaqueParameter(DynaqueParameter parameter) {
//        this.name = parameter.getName();
//        this.sort = parameter.getSort();
//        this.pageable = parameter.getPageable();
//        this.defaults = parameter.getDefaults();
//        this.overrides = parameter.getOverrides();
//        this.regulars = parameter.getRegulars();
//    }

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Pageable getPageable() {
        return pageable;
    }

    public void setPageable(Pageable pageable) {
        this.pageable = pageable;
    }

    public DefaultParameterValues getDefaults() {
        return defaults;
    }

    public void setDefaults(DefaultParameterValues defaults) {
        this.defaults = defaults;
    }

    public OverrideParameterValues getOverrides() {
        return overrides;
    }

    public void setOverrides(OverrideParameterValues overrides) {
        this.overrides = overrides;
    }

    public RegularParameterValues getRegulars() {
        return regulars;
    }

    public void setRegulars(RegularParameterValues regulars) {
        this.regulars = regulars;
    }
}
