package org.jote.util.dynaque;

import org.jote.util.dynaque.parametervalue.DefaultParameterValues;
import org.jote.util.dynaque.parametervalue.OverrideParameterValues;
import org.jote.util.dynaque.parametervalue.RegularParameterValues;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 11/7/12
 * Time: 2:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class DynaqueResult<T> {
    private DynaqueParameter parameter;
    private Long total;
    private List<T> content;

    public DynaqueResult() {

    }

    public DynaqueResult(String name,
                         Sort sort,
                         Pageable pageable,
                         DefaultParameterValues defaults,
                         OverrideParameterValues overrides,
                         RegularParameterValues regulars) {
        this.parameter = new DynaqueParameter(name, sort, pageable, defaults, overrides, regulars);

    }

    public DynaqueResult(DynaqueParameter parameter, Long total) {
        this.parameter = parameter;
        this.total = total;
    }

    public DynaqueResult(DynaqueParameter parameter, List<T> content) {
        this.parameter = parameter;
        this.content = content;
    }

    public DynaqueParameter getParameter() {
        return parameter;
    }

    public void setParameter(DynaqueParameter parameter) {
        this.parameter = parameter;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }
}
