package org.jote.util.dynaque;

import org.jote.util.dynaque.exception.UnknownQueryParameterLogicException;
import org.springframework.util.Assert;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 11/7/12
 * Time: 10:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class Dynautil {
    public static String getOperator(Operator operator) {
        String strOperator = "";
        Assert.notNull(operator, "Operator should be specified");
        switch (operator) {
            case LIKE:
                strOperator = "LIKE";
                break;
            case EQUAL:
                strOperator = "=";
                break;
            case STARTS_WITH:
                strOperator = "LIKE";
                break;
            case LESS_THAN:
                strOperator = "<";
                break;
            case LESS_THAN_EQUAL:
                strOperator = "<=";
                break;
            case GREATER_THAN:
                strOperator = ">";
                break;
            case GREATER_THAN_EQUAL:
                strOperator = ">=";
                break;
            case IN:
                strOperator = "in";
                break;
             case NOT_IN:
                strOperator = "not in";
                break;
            case IS:
                strOperator = "is";
                break;
        }
        return strOperator;
    }

    public static String getLogic(QueryParameterLogic logic) {
        String strLogic = "";
        Assert.notNull(logic, "Query parameter logic should be specified");
        switch (logic) {
            case AND:
                return "and";
            case OR:
                return "or";
        }
        throw new UnknownQueryParameterLogicException("specified Query parameter logic is unknown");
    }
}
