package org.jote.util.dynaque;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 11/7/12
 * Time: 10:13 AM
 * To change this template use File | Settings | File Templates.
 */
public enum Operator {
    LIKE,
    IN,
    NOT_IN,
    EQUAL,
    STARTS_WITH,
    ENDS_WIDTH,
    LESS_THAN,
    LESS_THAN_EQUAL,
    GREATER_THAN,
    GREATER_THAN_EQUAL,
    IS
}
