package org.jote.util.dynaque;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 11/7/12
 * Time: 4:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class ParameterValue {
    private String name;
    private Operator operator;
    private Object value;
    private Urgency urgency;

    public ParameterValue() {

    }

    public ParameterValue(String name, Operator operator, Object value) {
        this.name = name;
        this.operator = operator;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }


    public Urgency getUrgency() {
        return urgency;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    public static ParameterValue create(String name, Operator operator, Object value) {
        ParameterValue parameterValue = new ParameterValue();
        parameterValue.setName(name);
        parameterValue.setOperator(operator);
        parameterValue.setValue(value);
        return parameterValue;
    }
}
