package org.jote.util.dynaque;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 11/7/12
 * Time: 11:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class ParameterValues {
    private List<ParameterValue> values = new ArrayList<ParameterValue>();

    protected ParameterValues add(ParameterValue value) {
        values.add(value);
        return this;
    }

    public void gather(ParameterValues... values) {
        this.values.clear();
        for (int i = 0; i < values.length; i++) {
            ParameterValues value = values[i];
            this.values.addAll(value.getValues());
        }
    }

    public List<ParameterValue> getValues() {
        return values;
    }
}
