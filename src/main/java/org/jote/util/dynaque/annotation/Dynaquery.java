package org.jote.util.dynaque.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 11/6/12
 * Time: 6:30 PM
 * To change this template use File | Settings | File Templates.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Dynaquery {
    String name() default "";

    String base() default "";

    QueryParameter[] parameters() default {};
}
