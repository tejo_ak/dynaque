package org.jote.util.dynaque.annotation;

import org.jote.util.dynaque.NullMeans;
import org.jote.util.dynaque.QueryParameterLogic;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 11/6/12
 * Time: 6:46 PM
 * To change this template use File | Settings | File Templates.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface QueryParameter {
    String name() default "";

    String field() default "";

    NullMeans nullMeans() default NullMeans.IGNORE;

    QueryParameterLogic logic() default QueryParameterLogic.AND;

    String parent() default "root";

    QuerySubParameter[] subParameter() default {};
}
