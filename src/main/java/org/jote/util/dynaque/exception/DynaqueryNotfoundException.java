package org.jote.util.dynaque.exception;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 11/7/12
 * Time: 6:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class DynaqueryNotfoundException extends RuntimeException {
    public DynaqueryNotfoundException() {
    }

    public DynaqueryNotfoundException(String message) {
        super(message);
    }

    public DynaqueryNotfoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public DynaqueryNotfoundException(Throwable cause) {
        super(cause);
    }
}
