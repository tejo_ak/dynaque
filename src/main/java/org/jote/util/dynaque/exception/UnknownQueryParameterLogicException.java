package org.jote.util.dynaque.exception;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 11/7/12
 * Time: 6:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class UnknownQueryParameterLogicException extends RuntimeException {
    public UnknownQueryParameterLogicException() {
    }

    public UnknownQueryParameterLogicException(String message) {
        super(message);
    }

    public UnknownQueryParameterLogicException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownQueryParameterLogicException(Throwable cause) {
        super(cause);
    }
}
