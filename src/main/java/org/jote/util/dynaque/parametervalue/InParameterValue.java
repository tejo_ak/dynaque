package org.jote.util.dynaque.parametervalue;

import org.jote.util.dynaque.Operator;
import org.jote.util.dynaque.ParameterValue;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 11/7/12
 * Time: 10:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class InParameterValue extends ParameterValue {
    public InParameterValue(String name, Object value) {
        super(name, Operator.IN, value);
    }

    public static ParameterValue create(String name, Object value) {
        return ParameterValue.create(name, Operator.IN, value);
    }
}
