package org.jote.util.dynaque.parametervalue;

import org.jote.util.dynaque.ParameterValue;
import org.jote.util.dynaque.ParameterValues;
import org.jote.util.dynaque.Urgency;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 11/7/12
 * Time: 11:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class OverrideParameterValues extends ParameterValues {

    public OverrideParameterValues add(ParameterValue value) {
        value.setUrgency(Urgency.OVERRIDE);
        super.add(value);
        return this;
    }


}
