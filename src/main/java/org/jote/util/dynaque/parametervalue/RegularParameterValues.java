package org.jote.util.dynaque.parametervalue;

import org.jote.util.dynaque.ParameterValue;
import org.jote.util.dynaque.ParameterValues;
import org.jote.util.dynaque.Urgency;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 11/7/12
 * Time: 11:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class RegularParameterValues extends ParameterValues {

    public RegularParameterValues add(ParameterValue value) {
        value.setUrgency(Urgency.REGULAR);
        super.add(value);
        return this;
    }


}
